using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BallHandler : MonoBehaviour
{
    [SerializeField] private GameObject ballPrefab; // The prefab for the ball object.
    [SerializeField] private Rigidbody2D pivot; // The pivot point where the ball is connected.
    [SerializeField] private float respawnDelay; // Delay before respawning a new ball.
    [SerializeField] private float detachBalldelay; // Delay before detaching the ball.

    private Rigidbody2D currentBallRigibody; // The Rigidbody of the current ball.
    private SpringJoint2D currentBallSpringJoint2D; // The SpringJoint2D component of the current ball.
    private Camera mainCamera; // Reference to the main camera.
    private bool isDragging; // Flag to indicate if the player is currently dragging the ball.

    // Start is called before the first frame update
    void Start()
    {
        // Get a reference to the main camera.
        mainCamera = Camera.main;

        // Spawn the initial ball.
        SpawnNewBall();
    }

    // Update is called once per frame
    void Update()
    {
        // If there is no current ball, return early.
        if (currentBallRigibody == null) { return; }

        // Check if the primary touch is not pressed.
        if (!Touchscreen.current.primaryTouch.press.isPressed)
        {
            // If the player was dragging the ball, launch it.
            if (isDragging)
            {
                LaunchBall();
            }

            // Reset the dragging flag and return.
            isDragging = false;
            return;
        }

        // If the primary touch is pressed, the player is dragging the ball.
        isDragging = true;

        // Make the current ball kinematic (not affected by physics) while dragging.
        currentBallRigibody.isKinematic = true;

        // Get the current touch position in screen coordinates.
        Vector2 touchPosition = Touchscreen.current.primaryTouch.position.ReadValue();

        // Convert the screen position to world coordinates using the main camera.
        Vector3 worldPosition = mainCamera.ScreenToWorldPoint(touchPosition);

        // Set the position of the current ball to the touch position.
        currentBallRigibody.position = worldPosition;
        Debug.Log(worldPosition);
    }

    // Launch the current ball.
    private void LaunchBall()
    {
        // Make the current ball non-kinematic (affected by physics).
        currentBallRigibody.isKinematic = false;

        // Set the current ball reference to null (no current ball).
        currentBallRigibody = null;

        // Invoke the DetachBall method with a delay.
        Invoke(nameof(DetachBall), detachBalldelay);
    }

    // Detach the current ball from the pivot point.
    private void DetachBall()
    {
        // Disable the SpringJoint2D component to detach the ball.
        currentBallSpringJoint2D.enabled = false;

        // Set the current SpringJoint2D reference to null.
        currentBallSpringJoint2D = null;

        // Invoke the SpawnNewBall method to spawn a new ball with a delay.
        Invoke(nameof(SpawnNewBall), respawnDelay);
    }

    // Spawn a new ball at the pivot point.
    private void SpawnNewBall()
    {
        // Instantiate a new ball from the prefab at the pivot position with no rotation.
        GameObject ballInstance = Instantiate(ballPrefab, pivot.position, Quaternion.identity);

        // Get the Rigidbody2D component of the new ball.
        currentBallRigibody = ballInstance.GetComponent<Rigidbody2D>();

        // Get the SpringJoint2D component of the new ball.
        currentBallSpringJoint2D = ballInstance.GetComponent<SpringJoint2D>();

        // Connect the new ball to the pivot using the SpringJoint2D.
        currentBallSpringJoint2D.connectedBody = pivot;
    }
}
